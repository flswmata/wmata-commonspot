<cfoutput>			<h1>Search Results</h1>
			
			
			
			<div class="form-wrapper" id="archive-form-wrapper">
			
			
  			 
			 <div class="col-md-18">
			 	<label for="archive-list-search">KEYWORD</label>
				<input type="text" placeholder="Search Term" id="archive-list-search">
			 </div>
			 						
				<div class="col-md-24">
						<input id="submit-list-search" type="submit" value="SEARCH"> 
				</div><!-- col md 24 -->
				
			</div><! -- form wrapper -->
			
			<h4>Displaying results 1-10 of 50 for <span>keyword</span></h4>
			
			<ul class="archive-list">
				<li>
				<div class="archive-list-details">
				    <p class="list-details-date">September 27, 2015</p>
					<h3><a href="##">Metro plans additional service change at Stadium-Armory Station to further ease rush-hour congestion, delays on Orange, Blue & Silver lines</a></h3>
					<p>Sed finibus sit amet nisl ac interdum. Mauris venenatis elit sem, vel vulputate sem vehicula vel. Vestibulum euismod ultricies fringilla. Suspendisse vel nulla nisl. Donec nec tincidunt magna, sit amet semper ante.</p>
				</div>	
					</li>
					 
				<li>
				<div class="archive-list-details">
				<p class="list-details-date">September 25, 2015</p>
				<h3><a href="##">Metro announces temporary service change for Orange & Silver lines to ease congestion, delays in wake of transformer fire </a></h3>
				<p>Vestibulum vitae est sed justo eleifend molestie. Aliquam sollicitudin dictum augue nec maximus. Nulla facilisi. Vestibulum tristique elit eu dui rutrum, et egestas lectus viverra. Duis ipsum orci, tempus vel mauris eu, tincidunt porttitor elit.</p></div>	
					</li>
				<li>
				<div class="archive-list-details">
				<p class="list-details-date">September 21, 2015</p>
				<h3><a href="##">Metro restores service on Orange, Silver & Blue lines </a></h3>
				<p>Cras lobortis ipsum non libero venenatis, quis congue nisl interdum. Aenean mollis pulvinar est, sed luctus metus ornare et. Fusce fermentum volutpat nisi eu varius. Fusce lectus dui, dignissim sed rhoncus quis, interdum ut nisl.</p></div>	
					</li>
					 
				<li>
				<div class="archive-list-details">
				<p class="list-details-date">September 21, 2015</p>
				<h3><a href="##">Update on Orange/Silver/Blue line service</a></h3>
				<p>Proin bibendum, enim et volutpat ultricies, ante diam volutpat odio, eget aliquam ex sem vitae magna. Cras aliquam, mauris ac facilisis aliquam, odio risus aliquam risus, tempus pellentesque ipsum sapien at velit.</p></div>	
					</li>
					 
				<li>
				<div class="archive-list-details">
				<p class="list-details-date">September 18, 2015</p>
				<h3><a href="##">Aenean non metus diam.</a></h3>
				<p>Maecenas maximus neque ac ipsum mattis, at ullamcorper tellus feugiat. Duis maximus odio lectus, sit amet eleifend ex elementum vel. Integer vulputate nibh sit amet erat bibendum mattis.</p></div>	
					</li>
					 
				<li>
				<div class="archive-list-details">
				<p class="list-details-date">September 17, 2015</p>
				<h3><a href="##">Vivamus arcu felis, maximus non congue tincidunt, fringilla a est.</a></h3>
				<p>Cras tincidunt libero eu ipsum faucibus, ut tincidunt nunc aliquam. Fusce lobortis ultrices purus aliquam eleifend.</p></div>	
					</li>
					 
				<li>
				<div class="archive-list-details">
				<p class="list-details-date">September 17, 2015</p>
				<h3><a href="##">Maecenas interdum, est at aliquet laoreet, velit ante ultricies velit, non sollicitudin dolor massa aliquam augue.</a></h3>
				<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam a maximus neque, eget auctor mi. Quisque vitae mollis ante, nec dignissim sapien. Ut euismod ligula sed ipsum egestas aliquam.</p></div>	
					</li>
					 
				<li>
				<div class="archive-list-details">
				<p class="list-details-date">September 11, 2015</p>
				<h3><a href="##">Donec lacus dui, rutrum quis fermentum ut, tristique ut tellus. </a></h3>
				<p>Donec quis dui aliquam massa tempor convallis. Vestibulum finibus ligula eget lorem vehicula, vel accumsan quam auctor. Suspendisse potenti. Nullam vel est blandit, malesuada metus a, elementum quam.</p></div>	
					</li>
					 
				<li>
				<div class="archive-list-details">
				<p class="list-details-date">September 1, 2015</p>
				<h3><a href="##">Maecenas ut dictum arcu. Sed fringilla fringilla cursus. </a></h3>
				<p>Phasellus eget justo ut felis tristique convallis.Duis eleifend suscipit erat, et porta erat sodales ut. Curabitur at congue ex. Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut tristique felis ut blandit tincidunt.</p></div>	
					</li>
					 
				<li>
				<div class="archive-list-details">
				<p class="list-details-date">September 1, 2015</p>
				<h3><a href="##">Ut pellentesque vitae elit non consectetur.</a></h3>
				<p>Morbi aliquam condimentum diam nec eleifend. Pellentesque congue, turpis nec elementum feugiat, leo neque cursus est, in ornare nulla libero imperdiet quam. Suspendisse orci velit, efficitur in blandit in, condimentum vel ipsum.</p></div>	
					</li>
			
			
			</ul>
			
			
		  <ul class="pagination">
			<li class="prev disabled"><a href="##" aria-label="Previous"><span aria-hidden="true">&laquo; Prev</span></a></li>
			
			<li class="active"><a href="##">1 <span class="sr-only">(current)</span></a></li>
		
					  <li><a href="##">2</a></li>
					  <li><a href="##">3</a></li>
					  <li><a href="##">4</a></li>
					  <li><a href="##">5</a></li>
			<li class="next">
			  <a href="##" aria-label="Next">
				<span aria-hidden="true">Next &raquo;</span>
			  </a>
			</li>
		  </ul>
</cfoutput>