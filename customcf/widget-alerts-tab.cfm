<cfscript>
	// Setup Bus Alerts
	VARIABLES.busAlertsStruct 	= deserializeJSON(APPLICATION.busAlertsJSON);

	// Setup Bus Alerts Stats, etc.
	VARIABLES.busAlerts = {
		count 				= arrayLen(VARIABLES.busAlertsStruct.alerts),
		linesAffected = ""
	};

	for (theIndex=1; theIndex LTE VARIABLES.busAlerts.count; theIndex++) {

		VARIABLES.busAlerts.linesAffected = listSort(listRemoveDuplicates(listAppend(VARIABLES.busAlerts.linesAffected, replace(VARIABLES.busAlertsStruct.alerts[theIndex].title, " ", "", "ALL"))), "Text");

	};

	VARIABLES.busAlerts.linesAffectedCount = listLen(VARIABLES.busAlerts.linesAffected);


// Add Station Alert for Testing
foo = {};
structInsert(foo, "title", "Stadium-Armory");
structInsert(foo, "link", "http://www.wmata.com");
structInsert(foo, "description", "Orange, Silver &amp; Blue Line trains are now servicing Stadium-Armory until 2PM. ##wmata");
structInsert(foo, "source", "Alert");
structInsert(foo, "pubDate", "Tue, 20 Oct 2015 14:25:47 GMT");
structInsert(foo, "guid", "76A65AA4-66BE-4A48-8179-2204AEC72F43");
arrayAppend(VARIABLES.busAlertsStruct.alerts, foo);

	// Setup Bus Advisories
	VARIABLES.busAdvisoriesStruct 	= deserializeJSON(APPLICATION.busAdvisoriesJSON);

	// Setup Bus Advisories Stats, etc.
	VARIABLES.busAdvisories = {
		count 				= arrayLen(VARIABLES.busAdvisoriesStruct.advisories),
		linesAffected = ""
	};

	for (theIndex=1; theIndex LTE VARIABLES.busAdvisories.count; theIndex++) {

		VARIABLES.busAdvisories.linesAffected = listSort(listRemoveDuplicates(listAppend(VARIABLES.busAdvisories.linesAffected, replace(VARIABLES.busAdvisoriesStruct.advisories[theIndex].title, " ", "", "ALL"))), "Text");

	};

	VARIABLES.busAdvisories.linesAffectedCount = listLen(VARIABLES.busAdvisories.linesAffected);

	// Setup Train Alerts
	VARIABLES.trainAlertsStruct = deserializeJSON(APPLICATION.trainAlertsJSON);

	// Setup Train Alerts Stats, etc.
	VARIABLES.trainAlerts = {
		count 				= arrayLen(VARIABLES.trainAlertsStruct.alerts),
		linesAffected = ""
	};

	for (theIndex=1; theIndex LTE VARIABLES.trainAlerts.count; theIndex++) {

		VARIABLES.trainAlerts.linesAffected = listSort(listRemoveDuplicates(listAppend(VARIABLES.trainAlerts.linesAffected, replace(VARIABLES.trainAlertsStruct.alerts[theIndex].title, " ", "", "ALL"))), "Text");

	};

	VARIABLES.trainAlerts.linesAffectedCount = listLen(VARIABLES.trainAlerts.linesAffected);

	// Setup Train Advisories
	VARIABLES.trainAdvisoriesStruct = deserializeJSON(APPLICATION.trainAdvisoriesJSON);

	// Setup Train Advisories Stats, etc.
	VARIABLES.trainAdvisories = {
		count 				= arrayLen(VARIABLES.trainAdvisoriesStruct.advisories),
		linesAffected = ""
	};

	for (theIndex=1; theIndex LTE VARIABLES.trainAdvisories.count; theIndex++) {

		VARIABLES.trainAdvisories.linesAffected = listSort(listRemoveDuplicates(listAppend(VARIABLES.trainAdvisories.linesAffected, trim(VARIABLES.trainAdvisoriesStruct.advisories[theIndex].title))), "Text");

	};

	VARIABLES.trainAdvisories.linesAffectedCount = listLen(VARIABLES.trainAdvisories.linesAffected);
	
	//writeDump(var=VARIABLES.busAlertsStruct, label="VARIABLES.busAlertsStruct");
	//writeDump(var=VARIABLES.busAdvisoriesStruct, label="VARIABLES.busAdvisoriesStruct");
	//writeDump(var=VARIABLES.trainAlertsStruct, label="VARIABLES.trainAlertsStruct");
	//writeDump(var=VARIABLES.trainAdvisoriesStruct, label="VARIABLES.trainAdvisoriesStruct");
	//writeDump(var=VARIABLES.busAlerts, label="VARIABLES.busAlerts");
	//writeDump(var=VARIABLES.busAdvisories, label="VARIABLES.busAdvisories");
	//writeDump(var=VARIABLES.trainAlerts, label="VARIABLES.trainAlerts");
	//writeDump(var=VARIABLES.trainAdvisories, label="VARIABLES.trainAdvisories");

	param FORM.alertsAffecting 	= "both";
	param FORM.alertsType 		= "delay,alert";
</cfscript>

		
<!--INTERNAL PAGES SERVICES ALERT-->
<div class="inner-service-alerts">

	<div class="alerts-wrapper">	

		<div id="alerts-tab" class="side-tab-wrapper">

			 <div class="side-tab">
				 <div class="alert-count-wrapper" aria-label="View Alerts">
					 <div class="count">
						 <span></span>
						 <cfoutput>#val(VARIABLES.busAlerts.count + VARIABLES.trainAlerts.count)#</cfoutput>
					 </div> <!--.count-->
				</div> <!-- .alert-count-wrapper-->
			 </div>	 <!--.side-tab-->
				 
			 <div class="side-tab-panel">

				<div class="service-alerts open">

					<div class="alerts-header">
						<div class="alert-count-wrapper" aria-label="View All Alerts">
							<div class="count">
								<span></span>
								<cfoutput>#val(VARIABLES.busAlerts.count + VARIABLES.trainAlerts.count)#</cfoutput>
							</div> <!--.count-->
						</div> <!-- .alert-count-wrapper-->
						<div class="details-header">
							<div class="close-details">X</div>
							<div class="details-header-text">SERVICE ALERTS</div>
						</div> <!--. details-header-->
					</div> <!--.alerts-header-->
	
	 				<!--LIVE ALERTS CONTENT-->
	 				<cfoutput>
					<div class="alerts-content live-alerts simplebar">
						<ul>
							<cfloop from="1" to="#arrayLen(VARIABLES.busAlertsStruct.alerts)#" index="theAlertIndex">
								<cfloop from="1" to="#listLen(VARIABLES.busAlertsStruct.alerts[theAlertIndex].title)#" index="theBusIndex">
									<li>
										<div class="alert-icon bus"><span></span>#listGetAt(VARIABLES.busAlertsStruct.alerts[theAlertIndex].title, theBusIndex)#</div>
										<div class="alert-details">
											<p>#replaceNoCase(VARIABLES.busAlertsStruct.alerts[theAlertIndex].description, replace(VARIABLES.busAlertsStruct.alerts[theAlertIndex].title, " ", "") & ":", "")#</p>
										</div>
									</li>
								</cfloop>
							</cfloop>
							<cfloop from="1" to="#arrayLen(VARIABLES.trainAlertsStruct.alerts)#" index="theAlertIndex">
								<cfloop from="1" to="#listLen(VARIABLES.trainAlertsStruct.alerts[theAlertIndex].title)#" index="theTrainIndex">
									<li>
										<div class="alert-icon rail #trim(lCase(listGetAt(VARIABLES.trainAlertsStruct.alerts[theAlertIndex].title, theTrainIndex)))# text-capitalize"><span></span>#trim(lCase(listGetAt(VARIABLES.trainAlertsStruct.alerts[theAlertIndex].title, theTrainIndex)))# Rail Line</div> 
										<div class="alert-details">
											<p>#replaceNoCase(VARIABLES.trainAlertsStruct.alerts[theAlertIndex].description, replace(VARIABLES.trainAlertsStruct.alerts[theAlertIndex].title, " ", "") & " Line:", "")#</p>
										</div> 
									</li>
								</cfloop>
							</cfloop>
						</ul> 
					</div><!-- .alerts-content -->
					</cfoutput>

					<!--NO ALERTS CONTENT-->
					 <!-- <div class="alerts-content no-alerts-content">
								<ul>
									<li>
										<div class="alert-icon rail-no-alert"><span></span>RAIL</div>
										<div class="alert-details">
											 <p>On Time - There are no Metrorail alerts at this time.</p>
										</div>
									</li>
										
									<li>
										<div class="alert-icon bus"><span></span>BUS</div>
										<div class="alert-details">
											 <p>On Time - There are no Metrobus alerts at this time.</p>
										</div>
									</li>
								 </ul>
						</div> --><!--alerts-content no-alerts-content-->
					
					<div class="alert-footer">
						<div class="open-arrow" aria-label="View All Alerts"></div>
						<div class="alert-footer-content">
							<div>
								<a href="#lines-affected">View all status &amp; alerts</a>
							</div>

							<div>
								<p>Last Updated: <cfoutput>#timeFormat(VARIABLES.busAlertsStruct.timeStamp, "hh:mm tt")#</cfoutput></p>
							</div>
						</div> <!-- .opened-footer-content -->
					</div> <!-- .alert-footer -->            

				</div><!-- .service-alerts -->

			 </div> <!--.side-tab-panel-->

		</div>  <!--#alerts-tab-->  
		 <!--//// TRIP PLANNER TAB & CONTENT -->    
			<div id="trip-tab" class="side-tab-wrapper" aria-label="View Alerts">
				 <div class="side-tab">
					<span></span>
					 TRIP PLANNER
				 </div>
				 
				 <div class="side-tab-panel">
					<div class="panel-content">               
						<div class="panel-header">
							<div class="close-tab-panel">X</div>
								 <div class="panel-header-text">TRIP PLANNER</div>
							 </div> 
							<form class="form-horizontal" role="form">
								<div class="form-group location">
									 <div>
										 <label for="location">FROM</label>
										 <input type="text" id="location" placeholder="Address, Intersection, or Landmark" tabindex="1">
									 </div>
									 
									 <div>
										 <label for="destination">TO</label>
										 <input type="text" id="destination" placeholder="Address, Intersection, or Landmark" tabindex="2">
									 </div>
								 
									 <input id="submit-trip" type="submit" value="PLAN MY TRIP" tabindex="3"> 
										 
								 </div> 
							</form>
							<a href="#" class="more-arrow">ADVANCED SEARCH</a>
						</div>
					 </div>

				</div>  
		
				<!--//// NEXT BUS TAB & CONTENT -->    
				<div id="bus-tab" class="side-tab-wrapper">
					<div class="side-tab">
						<span></span>
						 NEXT BUS
				 	</div>
				 
				 	<div class="side-tab-panel">
						<div class="panel-content">               
							<div class="panel-header">
								<div class="close-tab-panel">X</div>
								<div class="panel-header-text">NEXT BUS</div>
							</div> 
							<form class="form-horizontal" role="form">
								<div class="form-group next-service">
									<p>By Location</p>
									<div>
										<label for="location-bus">Search for transportation near you</label>
										<input type="text" id="location-bus" placeholder="Address, Intersection, or Landmark" tabindex="1">
									</div>

									<input id="bus-routes" type="submit" value="SHOW" tabindex="2"> 

								</div> 
							</form>
							<a href="#" class="more-arrow">MORE OPTIONS</a>
						</div> 
					</div> 
				</div> 
		 
		 		<!--//// NEXT TRAIN TAB & CONTENT -->    
		 		<div id="train-tab" class="side-tab-wrapper">
			
					<div class="side-tab">
						<span></span>
						NEXT TRAIN
				 	</div>
				 	<div class="side-tab-panel">
						<div class="panel-content">
							<div class="panel-header">
								<div class="close-tab-panel">X</div>
								<div class="panel-header-text">NEXT TRAIN</div>
							</div> 

							<form class="form-horizontal" role="form">
								<p>Search for transportation near you</p>

								<div id="nearby-stations" class="search-option">

									<div class="form-group">
										<label for="search-nearby">Nearby Stations</label>
										<select id="search-nearby" tabindex="1">
											<option value="xxxx">Dupont Circle</option>
											<option value="xxxx">Farragut North</option>
											<option value="xxxx">Metro Center</option>
										</select> 

									</div> 

								</div> 

								<div id="all-stations" class="search-option">

									<div class="form-group">
										<label for="search-all">All Stations</label>
										<input type="text" id="search-all" placeholder="Search" tabindex="1">
									</div> 
								</div> 

								<input id="train-routes" type="submit" value="SHOW" tabindex="2"> 
							</form>

							<a href="#" class="more-arrow">MORE OPTIONS</a>
						</div> 
					</div> 
				</div> 
			</div>	
		</div> <!-- .inner-service-alerts-->
		
		<div class="hero-wrapper">
			<div class="hero">
				<div class="hero-img"></div>
				<!-- .hero-img -->
			</div><!-- .hero -->
		</div><!-- .hero-wrapper -->

