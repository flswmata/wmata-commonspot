<!--- 
Purpose: 		I take the Alert RSS feeds for Rail and Bus from MIS Science and convert it into JSON Objects 

Author's Name:	Christian N. Abad
  
Date Initially Written:	08/25/2015

History:
Date		By					Modification
08/25/2015 	Christian N. Abad 	Original Build
--->

<!---
RSS Feed for ALL Alerts:
http://www.metroalerts.info/rss.aspx

RSS Feed for ALL Advisories:
(TODO)

RSS Feed for Bus Alerts:
http://www.metroalerts.info/rss.aspx?bus

RSS Feed for Bus Advisories (TODO: Determine source for this...):
http://www.wmata.com/rider_tools/metro_service_status/feeds/bus_Advisories.xml

RSS Feed for Train Alerts:
http://www.metroalerts.info/rss.aspx?rs

RSS Feed for Train Advisories (TODO: Determine source for this...):
http://www.wmata.com/rider_tools/metro_service_status/feeds/rail_Advisories.xml
--->

<cfparam name="URL.debug" default="0" />

<cfif URL.debug>
	<h1>RSS Feeds</h1>
</cfif>

<!--- Bus Alerts --->
<cfhttp method="get" url="http://www.metroalerts.info/rss.aspx?bus" result="VARIABLES.rssBusAlerts" />

<cfscript>
	if (VARIABLES.rssBusAlerts.statusCode contains "200") {
		if (isXML(VARIABLES.rssBusAlerts.fileContent)) {
			VARIABLES.busAlertsXML 				= xmlSearch(VARIABLES.rssBusAlerts.fileContent, "/rss/channel");
			VARIABLES.busAlertsStruct 			= structNew();
			VARIABLES.busAlertsStruct.timeStamp = now();
			VARIABLES.busAlertsStruct.alerts 	= arrayNew(1);
			for (itemIndex = 1; itemIndex <= arrayLen(VARIABLES.busAlertsXML[1].item); itemIndex ++) {
				arrayAppend(VARIABLES.busAlertsStruct.alerts, APPLICATION.utils.xmlToStruct(VARIABLES.busAlertsXML[1].item[itemIndex]));	
			}
			APPLICATION.busAlertsJSON 			= serializeJSON(VARIABLES.busAlertsStruct);
			writeOutput("<p style='color: green;'>APPLICATION.busAlertsJSON updated at " & now() & "</p>");
		} else {
			VARIABLES.busAlertsStruct 			= structNew();
			VARIABLES.busAlertsStruct.timeStamp = now();
			VARIABLES.busAlertsStruct.alerts 	= arrayNew(1);
			APPLICATION.busAlertsJSON 			= serializeJSON(VARIABLES.busAlertsStruct);
			writeOutput("<p style='color: green;'>APPLICATION.busAlertsJSON reset to empty</p>");
		}
	} else {
		writeOutput("<p style='color: red;'>APPLICATION.busAlertsJSON NOT updated</p>");
	}
</cfscript>

<cfif URL.debug and len(VARIABLES.rssBusAlerts.fileContent)>
	<h2>Bus Alerts:</h2>
	<h3>Bus Alerts XML from MIS Services:</h3>
	<cfdump var="#xmlParse(VARIABLES.rssBusAlerts.fileContent)#" label="Bus Alerts XML from MIS Services" />
	<br />
	<br />
	<h3>Bus Alerts "Channel" Node from XML:</h3>
	<cfdump var="#VARIABLES.busAlertsXML[1]#" label="Bus Alerts 'Channel' Node from XML" top="1" />
	<br />
	<br />
	<h3>Bus Alerts "Channel" Node XML Converted to Struct:</h3>
	<cfdump var="#VARIABLES.busAlertsStruct#" label="Bus Alerts Channel Node XML Converted to Struct" />
	<br />
	<br />
	<h3>Bus Alerts Struct Converted to JSON (APPLICATION.busAlertsJSON):</h3>
	<cfoutput>#APPLICATION.busAlertsJSON#</cfoutput>
	<br />
	<br />
	<br />
	<br />
	<hr />
	<br />
	<br />
</cfif>

<!--- Bus Advisories --->
<cfhttp method="get" url="http://www.wmata.com/rider_tools/metro_service_status/feeds/bus_Advisories.xml" result="rssBusAdvisories" />

<cfscript>
	if (VARIABLES.rssBusAdvisories.statusCode contains "200") {
		if (isXML(VARIABLES.rssBusAdvisories.fileContent)) {
			VARIABLES.busAdvisoriesXML 					= xmlSearch(VARIABLES.rssBusAdvisories.fileContent, "/rss/channel");
			VARIABLES.busAdvisoriesStruct 				= structNew();
			VARIABLES.busAdvisoriesStruct.timeStamp 	= now();
			VARIABLES.busAdvisoriesStruct.advisories 	= arrayNew(1);
			for (itemIndex = 1; itemIndex <= arrayLen(VARIABLES.busAdvisoriesXML[1].item); itemIndex ++) {
				arrayAppend(VARIABLES.busAdvisoriesStruct.advisories, APPLICATION.utils.xmlToStruct(VARIABLES.busAdvisoriesXML[1].item[itemIndex]));
			}
			APPLICATION.busAdvisoriesJSON 				= serializeJSON(VARIABLES.busAdvisoriesStruct);
			writeOutput("<p style='color: green;'>APPLICATION.busAdvisoriesJSON updated at " & now() & "</p>");
		} else {
			VARIABLES.busAdvisoriesStruct 				= structNew();
			VARIABLES.busAdvisoriesStruct.timeStamp 	= now();
			VARIABLES.busAdvisoriesStruct.advisories 	= arrayNew(1);
			APPLICATION.busAdvisoriesJSON 				= serializeJSON(VARIABLES.busAdvisoriesStruct);
			writeOutput("<p style='color: green;'>APPLICATION.busAdvisoriesJSON reset to empty</p>");
		}

	} else {
		writeOutput("<p style='color: red;'>APPLICATION.busAdvisoriesJSON NOT updated</p>");
	}
</cfscript>

<cfif URL.debug>
	<h2>Bus Advisories:</h2>
	<h3>Bus Advisories XML from MIS Services:</h3>
	<cfdump var="#xmlParse(rssBusAdvisories.fileContent)#" label="Bus Advisories XML from MIS Services" />
	<br />
	<br />
	<h3>Bus Advisories "Channel" Node from XML:</h3>
	<cfdump var="#VARIABLES.busAdvisoriesXML[1]#" label="Bus Advisories 'Channel' Node from XML" top="1" />
	<br />
	<br />
	<h3>Bus Advisories "Channel" Node XML Converted to Struct:</h3>
	<cfdump var="#VARIABLES.busAdvisoriesStruct#" label="Bus Advisories Channel Node XML Converted to Struct" />
	<br />
	<br />
	<h3>Bus Advisories Struct Converted to JSON (APPLICATION.busAdvisoriesJSON):</h3>
	<cfoutput>#APPLICATION.busAdvisoriesJSON#</cfoutput>
	<br />
	<br />
	<br />
	<br />
	<hr />
	<br />
	<br />
</cfif>

<!--- Train Alerts --->
<cfhttp method="get" url="http://www.metroalerts.info/rss.aspx?rs" result="rssTrainAlerts" />

<cfscript>
	if (VARIABLES.rssTrainAlerts.statusCode contains "200") {
		if (isXML(VARIABLES.rssTrainAlerts.fileContent)) {
			VARIABLES.trainAlertsXML 				= xmlSearch(VARIABLES.rssTrainAlerts.fileContent, "/rss/channel");
			VARIABLES.trainAlertsStruct 			= structNew();
			VARIABLES.trainAlertsStruct.timeStamp 	= now();
			VARIABLES.trainAlertsStruct.alerts 		= arrayNew(1);
			for (itemIndex = 1; itemIndex <= arrayLen(VARIABLES.trainAlertsXML[1].item); itemIndex ++) {
				arrayAppend(VARIABLES.trainAlertsStruct.alerts, APPLICATION.utils.xmlToStruct(VARIABLES.trainAlertsXML[1].item[itemIndex]));
			}
			APPLICATION.trainAlertsJSON 			= serializeJSON(VARIABLES.trainAlertsStruct);
			writeOutput("<p style='color: green;'>APPLICATION.trainAlertsJSON updated at " & now() & "</p>");
		} else {
			VARIABLES.trainAlertsStruct 			= structNew();
			VARIABLES.trainAlertsStruct.timeStamp 	= now();
			VARIABLES.trainAlertsStruct.alerts 		= arrayNew(1);
			APPLICATION.trainAlertsJSON 			= serializeJSON(VARIABLES.trainAlertsStruct);
			writeOutput("<p style='color: green;'>APPLICATION.trainAlertsJSON reset to empty</p>");
		}

	} else {
		writeOutput("<p style='color: red;'>APPLICATION.trainAlertsJSON NOT updated</p>");
	}
</cfscript>

<cfif URL.debug and len(VARIABLES.rssTrainAlerts.fileContent)>
	<h2>Train Alerts:</h2>
	<h3>Train Alerts XML from MIS Services:</h3>
	<cfdump var="#xmlParse(VARIABLES.rssTrainAlerts.fileContent)#" label="Train Alerts XML from MIS Services" />
	<br />
	<br />
	<h3>Train Alerts "Channel" Node from XML:</h3>
	<cfdump var="#VARIABLES.trainAlertsXML[1]#" label="Train Alerts 'Channel' Node from XML" top="1" />
	<br />
	<br />
	<h3>Train Alerts "Channel" Node XML Converted to Struct:</h3>
	<cfdump var="#VARIABLES.trainAlertsStruct#" label="Train Alerts Channel Node XML Converted to Struct" />
	<br />
	<br />
	<h3>Train Alerts Struct Converted to JSON (APPLICATION.trainAlertsJSON):</h3>
	<cfoutput>#APPLICATION.trainAlertsJSON#</cfoutput>
	<br />
	<br />
	<br />
	<br />
	<hr />
	<br />
	<br />
</cfif>

<!--- Train Advisories --->
<cfhttp method="get" url="http://www.wmata.com/rider_tools/metro_service_status/feeds/rail_Advisories.xml" result="rssTrainAdvisories" />

<cfscript>
	if (VARIABLES.rssTrainAdvisories.statusCode contains "200") {
		if (isXML(VARIABLES.rssTrainAdvisories.fileContent)) {
			VARIABLES.trainAdvisoriesXML 					= xmlSearch(VARIABLES.rssTrainAdvisories.fileContent, "/rss/channel");
			VARIABLES.trainAdvisoriesStruct 				= structNew();
			VARIABLES.trainAdvisoriesStruct.timeStamp 		= now();
			VARIABLES.trainAdvisoriesStruct.advisories 		= arrayNew(1);
			for (itemIndex = 1; itemIndex <= arrayLen(VARIABLES.trainAdvisoriesXML[1].item); itemIndex ++) {
				arrayAppend(VARIABLES.trainAdvisoriesStruct.advisories, APPLICATION.utils.xmlToStruct(VARIABLES.trainAdvisoriesXML[1].item[itemIndex]));
			}
			APPLICATION.trainAdvisoriesJSON 				= serializeJSON(VARIABLES.trainAdvisoriesStruct);
			writeOutput("<p style='color: green;'>APPLICATION.trainAdvisoriesJSON updated at " & now() & "</p>");
		} else {
			VARIABLES.trainAdvisoriesStruct 				= structNew();
			VARIABLES.trainAdvisoriesStruct.timeStamp 		= now();
			VARIABLES.trainAdvisoriesStruct.advisories 		= arrayNew(1);
			APPLICATION.trainAdvisoriesJSON 				= serializeJSON(VARIABLES.trainAdvisoriesStruct);
			writeOutput("<p style='color: green;'>APPLICATION.trainAdvisoriesJSON reset to empty</p>");
		}

	} else {
		writeOutput("<p style='color: red;'>APPLICATION.trainAdvisoriesJSON NOT updated</p>");
	}
</cfscript>

<cfif URL.debug and len(VARIABLES.rssTrainAdvisories.fileContent)>
	<h2>Train Advisories:</h2>
	<h3>Train Advisories XML from MIS Services:</h3>
	<cfdump var="#xmlParse(VARIABLES.rssTrainAdvisories.fileContent)#" label="Train Advisories XML from MIS Services" />
	<br />
	<br />
	<h3>Train Advisories "Channel" Node from XML:</h3>
	<cfdump var="#VARIABLES.trainAdvisoriesXML[1]#" label="Train Advisories 'Channel' Node from XML" top="1" />
	<br />
	<br />
	<h3>Train Advisories "Channel" Node XML Converted to Struct:</h3>
	<cfdump var="#VARIABLES.trainAdvisoriesStruct#" label="Train Advisories Channel Node XML Converted to Struct" />
	<br />
	<br />
	<h3>Train Advisories Struct Converted to JSON (APPLICATION.trainAdvisoriesJSON):</h3>
	<cfoutput>#APPLICATION.trainAdvisoriesJSON#</cfoutput>
	<br />
	<br />
	<br />
	<br />
	<hr />
	<br />
	<br />
</cfif>