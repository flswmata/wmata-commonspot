<cfscript>
	component {
	
		/**
		* @author 		Raymond Camden
		* @see 			http://www.raymondcamden.com/index.cfm/2012/1/4/Converting-XML-to-JSON--My-exploration-into-madness
		* @returntype 	Struct
		*/
		public function xmlToStruct(required xml x) hint = "I convert an XML Object into a CF Struct" {
			var s = {};
		 
		    if(xmlGetNodeType(x) == "DOCUMENT_NODE") {
		        s[structKeyList(x)] = xmlToStruct(x[structKeyList(x)]);    
		    }
		 
		    if(structKeyExists(x, "xmlAttributes") && !structIsEmpty(x.xmlAttributes)) { 
		        s.attributes = {};
		        for(var item in x.xmlAttributes) {
		            s.attributes[item] = x.xmlAttributes[item];        
		        }
		    }
		    
		    if(structKeyExists(x, "xmlText") && len(trim(x.xmlText))) {
		      s.value = x.xmlText;
		    }
		 
		    if(structKeyExists(x, "xmlChildren") && arrayLen(x.xmlChildren)) {
		        for(var i=1; i<=arrayLen(x.xmlChildren); i++) {
		            if(structKeyExists(s, x.xmlchildren[i].xmlname)) { 
		                if(!isArray(s[x.xmlChildren[i].xmlname])) {
		                    var temp = s[x.xmlchildren[i].xmlname];
		                    s[x.xmlchildren[i].xmlname] = [temp];
		                }
		                arrayAppend(s[x.xmlchildren[i].xmlname], xmlToStruct(x.xmlChildren[i]));                
		             } else {
		                 if(structKeyExists(x.xmlChildren[i], "xmlChildren") && arrayLen(x.xmlChildren[i].xmlChildren)) {
		                        s[x.xmlChildren[i].xmlName] = xmlToStruct(x.xmlChildren[i]);
		                 } else if(structKeyExists(x.xmlChildren[i],"xmlAttributes") && !structIsEmpty(x.xmlChildren[i].xmlAttributes)) {
		                    s[x.xmlChildren[i].xmlName] = xmlToStruct(x.xmlChildren[i]);
		                } else {
		                    s[x.xmlChildren[i].xmlName] = x.xmlChildren[i].xmlText;
		                }
		             }
		        }
		    }
		    
		    return s;
		};
		
	}
</cfscript>